def inter(list_a, list_b):
    return [value for value in list_a if value in list_b]

def sub(list_a, list_b):
    return [value for value in list_a if value not in list_b]

with open("input8.txt") as input:
    samples = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        pattern, value = line.split('|')
        pattern = pattern.split()
        value = value.split()
        samples.append((pattern, value))
    print(samples)
    total = 0
    digits = [None]*10
    answer = 0
    for s in samples:

        ## looking for 1 :
        for sig in s[0]:
            if len(sig) == 2:
                digits[1] = sig
                break
        s[0].remove(digits[1])

        ## looking for 4 :
        for sig in s[0]:
            if len(sig) == 4:
                digits[4] = sig
                break
        s[0].remove(digits[4])

        ## looking for 7 :
        for sig in s[0]:
            if len(sig) == 3:
                digits[7] = sig
                break
        s[0].remove(digits[7])

        ## looking for 8 :
        for sig in s[0]:
            if len(sig) == 7:
                digits[8] = sig
                break
        s[0].remove(digits[8])

        ## looking for 3 :
        for sig in s[0]:
            if len(sig) == 5 and len(inter(sig, digits[1])) == 2:
                digits[3] = sig
                break
        s[0].remove(digits[3])

        bd = sub(digits[4], digits[1])

        ## looking for 5
        for sig in s[0]:
            if len(sig) == 5 and len(inter(sig, bd)) == 2:
                digits[5] = sig
                break
        s[0].remove(digits[5])

        ## looking for 2
        for sig in s[0]:
            if len(sig) == 5:
                digits[2] = sig
                break
        s[0].remove(digits[2])

        ## looking for 9
        for sig in s[0]:
            if len(inter(sig, digits[3])) == 5:
                digits[9] = sig
                break
        s[0].remove(digits[9])

        ## looking for 0
        for sig in s[0]:
            if len(inter(sig, digits[7])) == 3:
                digits[0] = sig
                break
        s[0].remove(digits[0])

        ## looking for 6
        digits[6] = s[0][0]

        dico = dict()
        for i,v in enumerate(digits):
            dico[''.join(sorted(v))] = i
            print(i,v)
        value_str = ""
        for v in s[1]:
            value_str += str(dico[''.join(sorted(v))])
        answer += int(value_str)
    print(answer)
