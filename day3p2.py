
def find_rating(data, index, o2):

    if(len(data) == 1):
        return data[0]

    o = []
    z = []
    for line in data:
        if line [index] == '1' :
            o.append(line)
        else:
            z.append(line)

    select = None
    
    if len(o) >= len(z):
        if o2:
            select = o
        else:
            select = z
    else:
        if o2:
            select = z
        else:
            select = o

    return find_rating(select, index+1, o2)



with open("input3.txt") as input:
    bits = None
    byte_length = None
    entries = []
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            break
        entries.append(line)
        
    

    gamma_rate = 0
    epsilon_rate = 0
    o2 = int(find_rating(entries, 0, True),2)
    co2 = int(find_rating(entries, 0, False),2)
    
    print("{} x {} = {}".format(o2, co2, o2*co2))

