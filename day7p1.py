def median(l):
    l_len = len(l)
    if l_len < 1:
        return
    l = sorted(l)
    if l_len % 2 == 0 :
        return ( l[(l_len-1)//2] + l[(l_len+1)//2] ) // 2
    else:
        return l[(l_len-1)/2]


with open("input7.txt") as input:
    h_pos = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        h_pos = list(map(int,line.split(",")))
    h_median = median(h_pos)
    move = 0
    for p in h_pos:
        move += abs(h_median-p)
    print(move)