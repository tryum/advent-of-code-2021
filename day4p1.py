with open("input4.txt") as input:
    draws = list(map(int,input.readline().split(',')))
    input.readline()
    boards = []

    while True:

        board = [None]*10
        for i in range(5):
            board[i] = list(map(int,input.readline().split()))
            for j in range(5):
                if board[5+j] == None:
                    board[5+j] = []
                board[5+j].append(board[i][j])
        boards.append(board)

        if not input.readline():
            break

    for n in draws:
        for i, board in enumerate(boards):
            bingo = False
            for line in board:
                if line.count(n) > 0:
                    line.remove(n)
                if len(line) == 0:
                    bingo = True
            if bingo:
                board_numbers = set()
                for line in board:
                    for c in line:
                        board_numbers.add(c)
                sum = 0
                for c in board_numbers:
                    sum += c
                sum *= n
                print(sum)
                exit()