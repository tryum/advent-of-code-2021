with open("input4.txt") as input:
    draws = list(map(int,input.readline().split(',')))
    input.readline()
    boards = []

    while True:

        board = [None]*10
        for i in range(5):
            board[i] = list(map(int,input.readline().split()))
            for j in range(5):
                if board[5+j] == None:
                    board[5+j] = []
                board[5+j].append(board[i][j])
        boards.append(board)

        if not input.readline():
            break

    for n in draws:
        boards_to_remove = []
        for i, board in enumerate(boards):
            bingo = False
            for line in board:
                if line.count(n) > 0:
                    line.remove(n)
                if len(line) == 0:
                    bingo = True
            if bingo:
                if len(boards) == 1:
                    board_numbers = set()
                    for line in boards[0]:
                        for c in line:
                            board_numbers.add(c)
                    sum = 0
                    for c in board_numbers:
                        sum += c
                    sum *= n
                    print(sum)
                    exit()
                boards_to_remove.append(i)

        for i, x in enumerate(boards_to_remove):
            del boards[x-i]
        
        