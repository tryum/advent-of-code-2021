with open("input12.txt") as input:
    g = dict()
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            break
        a, b = line.split('-')
        if a not in g:
            g[a] = [b]
        else:
            g[a].append(b)
        if b not in g:
            g[b] = [a]
        else:
            g[b].append(a)

    visited = []
    queue = ["start"]

    all_paths = []

    while queue:
        path = queue.pop()
        last_node = path.split("-")[-1]
        for n in g[last_node]:
            if n == "end":
                all_paths.append(path+"-end")
            elif n.islower() and path.find(n) == -1:
                queue.append(path+'-'+n)
            elif n.isupper(): #should do cycle detection here
                queue.append(path+'-'+n)
        pass
    print(len(all_paths))
