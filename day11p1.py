with open("input11.txt") as input:
    octo = []
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            break
        octo.append([int(val) for val in line])

    flash_count = 0

    for step in range(100):
        for i in range(10):
            for j in range(10):
                octo[i][j] += 1

        new_flash = True
        while new_flash :
            new_flash = False
            for i in range(10):
                for j in range(10):
                    if octo[i][j] > 9 :
                        flash_count += 1
                        new_flash = True
                        octo[i][j] = 0
                        for k in [(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]:
                            if 0 <= i+k[0] < 10 and 0 <= j+k[1] < 10 and octo[i+k[0]][j+k[1]]:
                                octo[i+k[0]][j+k[1]] += 1

        print(step+1, flash_count)
