with open("inputd1p1.txt") as input:
    count = 0
    prev = None
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        depth = int(line)
        if prev != None and prev < depth:
            count += 1
        prev = depth
    print("{}".format(count))
