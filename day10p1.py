with open("input10.txt") as input:
    nav = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        nav.append(line.strip())
    print(nav)
    score_value_1 = {')': 3, ']': 57, '}': 1197, '>': 25137}
    score_value_2 = {')': 1, ']': 2, '}': 3, '>': 4}
    closing = {'(':')', '[': ']', '{': '}', '<': '>'}
    score_1 = 0
    score_2 = []
    incomplete = []
    for l in nav:
        stack = []
        for c in l:
            if c in closing:
                stack.append(closing[c])
            elif c == stack[-1]:
                stack.pop()
            else:
                score_1 += score_value_1[c]
                stack.clear()
                break
        if stack:
            local_score = 0
            stack.reverse()
            for c in stack:
                local_score *= 5
                local_score += score_value_2[c]
            score_2.append(local_score)
            
    print(score_1)
    score_2.sort()
    print(score_2[len(score_2)//2-1])
    print(score_2[len(score_2)//2])
    print(score_2[len(score_2)//2+1])

