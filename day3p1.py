with open("input3.txt") as input:
    bits = None
    entries = 0
    byte_length = None
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            break
        entries += 1
        if byte_length == None :
            byte_length = len(line)
            bits = [0]*byte_length
        for i, b in enumerate(line):
            if b == '1':
                bits[i] += 1
    gamma_rate = 0
    epsilon_rate = 0
    for i in range(byte_length):
        if bits[i] > entries / 2 :
            gamma_rate |= 1 << byte_length-1-i
        else:
            epsilon_rate |= 1 << byte_length-1-i

    
    print("{} x {} = {}".format(gamma_rate, epsilon_rate, gamma_rate*epsilon_rate))

