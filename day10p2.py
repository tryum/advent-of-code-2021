with open("input10s.txt") as input:
    nav = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        nav.append(line.strip())
    print(nav)
    score_value = {')': 3, ']': 57, '}': 1197, '>': 25137}
    closing = {'(':')', '[': ']', '{': '}', '<': '>'}
    score = 0
    for l in nav:
        stack = []
        for c in l:
            if c in closing:
                stack.append(closing[c])
            elif c == stack[-1]:
                stack.pop()
            else:
                score += score_value[c]
                break
    print(score)

