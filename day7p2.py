def median(l):
    l_len = len(l)
    if l_len < 1:
        return
    l = sorted(l)
    if l_len % 2 == 0 :
        return ( l[(l_len-1)//2] + l[(l_len+1)//2] ) // 2
    else:
        return l[(l_len-1)/2]


with open("input7.txt") as input:
    h_pos = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        h_pos = list(map(int,line.split(",")))
    h_median = median(h_pos)
    avg = sum(h_pos)/len(h_pos)
    pos_dist = dict()
    move = 0
    for p in h_pos:
            dist = abs(avg-p)
            move += (dist*(dist+1))/2
    print(move)
    for i in range (int(avg)-10, int(avg)+10):
        move = 0
        for p in h_pos:
            dist = abs(i-p)
            move += (dist*(dist+1))/2
        pos_dist[i] = move
    print(pos_dist)
    pos = list(pos_dist.values())
    pos.sort()
    print(pos)

# 93397632.0
# 93397545.47200012