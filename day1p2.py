with open("inputd1p2.txt") as input:
    count = 0
    depth=[]
    while True:
        line = input.readline()
        
        if len(line) == 0:
            break
        depth.append(int(line))
    for i in range(len(depth)):
        if i+3 < len(depth) and depth[i]<depth[i+3]:
            count += 1

    print("{}".format(count))

