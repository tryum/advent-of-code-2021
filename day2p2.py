with open("inputd2.txt") as input:
    y=0
    z=0
    d=0
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        if line.startswith("forward"):
            y += int(line.split(" ")[1])
            z += d*int(line.split(" ")[1])
        elif line.startswith("down"):
            d += int(line.split(" ")[1])
        elif line.startswith("up"):
            d -= int(line.split(" ")[1])
    print("{}x{}={}".format(y, z, y*z))
