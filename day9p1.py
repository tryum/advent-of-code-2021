from typing import Tuple


with open("input9.txt") as input:
    heightmap = []
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            break
        heightmap.append([int(val) for val in line])
    height = len(heightmap)
    width = len(heightmap[0])
    low_points = []
    for i in range(height):
        for j in range(width):
            if i > 0 and heightmap[i-1][j] <= heightmap[i][j]:
                continue
            if i < height-1 and heightmap[i+1][j] <= heightmap[i][j]:
                continue
            if j > 0 and heightmap[i][j-1] <= heightmap[i][j]:
                continue
            if j < width-1 and heightmap[i][j+1] <= heightmap[i][j]:
                continue
            low_points.append(heightmap[i][j])
    print(sum(low_points)+len(low_points))
