with open("input8.txt") as input:
    samples = []
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        pattern, value = line.split('|')
        pattern = pattern.split()
        value = value.split()
        samples.append((pattern, value))
    print(samples)
    total = 0
    for s in samples:
        count = 0
        for j in s[1]:
            sig_len = len(j)
            if sig_len == 2 or sig_len == 4 or sig_len == 3 or sig_len == 7:
                count += 1
        print("{} | {} : {}".format(" ".join(s[0]), " ".join(s[1]), count))
        total += count
    print(total)
