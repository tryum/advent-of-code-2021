with open("input5.txt") as input:
    lines = []
    max_coord = (0,0)
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        line = line.split()
        start = tuple(map(int,line[0].split(",")))
        end = tuple(map(int,line[2].split(",")))
        if start[0] > max_coord[0]:
            max_coord = (start[0], max_coord[1])
        if start[1] > max_coord[1]:
            max_coord = (max_coord[0], start[1])
        if end[0] > max_coord[0]:
            max_coord = (end[0], max_coord[1])
        if end[1] > max_coord[1]:
            max_coord = (max_coord[0], end[1])
        lines.append((start,end))
    overlap = [[0 for x in range(max_coord[0]+1)] for y in range(max_coord[1]+1)]
    for l in lines:
        if l[0][0] == l[1][0]:
            start, end = l[0][1], l[1][1]
            if start > end:
                start, end = end, start
            for y in range(start, end+1):
                overlap[y][l[0][0]] += 1
        elif l[0][1] == l[1][1]:
            start, end = l[0][0], l[1][0]
            if start > end:
                start, end = end, start
            for x in range(start, end+1):
                overlap[l[0][1]][x] += 1
        else:
            inc = (l[1][0] - l[0][0], l[1][1] - l[0][1])
            inc = (inc[0]//abs(inc[0]),inc[1]//abs(inc[1]))
            cell = l[0]
            while cell != l[1]:
                overlap[cell[1]][cell[0]] += 1
                cell = (cell[0]+inc[0], cell[1]+inc[1])
            overlap[cell[1]][cell[0]] += 1
            pass

    count = 0
    for y in range(len(overlap)):
        for x in range(len(overlap[y])):
            if overlap[y][x] > 1:
                count += 1

    print(count)