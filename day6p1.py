with open("input6.txt") as input:
    while True:
        line = input.readline()
        if len(line) == 0:
            break
        fishes = list(map(int,line.split(",")))
    print("Initial state: {}".format(fishes))
    ages = [0] * 9
    for f in fishes:
        ages[f] += 1
    for i in range(256):
        new_ages = [0]*9
        for a in range(9):
            if a == 0:
                new_ages[6] = ages[0]
                new_ages[8] = ages[0]
            else:
                new_ages[a-1] += ages[a]
        ages = new_ages
        if i == 79:
            print(sum(ages))
        if i == 255:
            print(sum(ages))
