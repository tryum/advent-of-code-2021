def IsValid(heightmap, loc):
    height = len(heightmap)
    width = len(heightmap[0])
    if 0 <= loc[0] < height and 0 <= loc[1] < width:
        return True
    else:
        return False

def BassinSize(heightmap, loc):
    visited = []
    queue = []

    visited.append(loc)
    queue.append(loc)
    while queue:
        m = queue.pop(0)
        neighbours = [(m[0],m[1]-1),(m[0]-1,m[1]),(m[0],m[1]+1),(m[0]+1,m[1])]
        for n in neighbours:
            if IsValid(heightmap, n) and not n in visited and heightmap[n[0]][n[1]] > heightmap[m[0]][m[1]] and heightmap[n[0]][n[1]] < 9:
                visited.append(n)
                queue.append(n)
    return len(visited)


with open("input9.txt") as input:
    heightmap = []
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            break
        heightmap.append([int(val) for val in line])
    height = len(heightmap)
    width = len(heightmap[0])
    low_points = []
    for i in range(height):
        for j in range(width):
            if i > 0 and heightmap[i-1][j] <= heightmap[i][j]:
                continue
            if i < height-1 and heightmap[i+1][j] <= heightmap[i][j]:
                continue
            if j > 0 and heightmap[i][j-1] <= heightmap[i][j]:
                continue
            if j < width-1 and heightmap[i][j+1] <= heightmap[i][j]:
                continue
            low_points.append((i,j))
    print(low_points)
    bassins = []
    for loc in low_points:
        bassins.append(BassinSize(heightmap, loc))
    bassins.sort()
    print(bassins[-1]*bassins[-2]*bassins[-3])