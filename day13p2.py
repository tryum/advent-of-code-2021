with open("input13.txt") as input:
    dots = set()
    splits = []
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            break
        dots.add(tuple([int(val) for val in line.split(',')]))
    while True:
        line = input.readline().strip()
        if len(line) == 0:
            break
        line = line.split()
        axis, value = line[-1].split('=')
        splits.append((axis,int(value)))
    print(dots)
    print(splits)
    for s in splits:
        new_dots = set()
        if s[0] == 'x':
            for d in dots:
                if d[0] > s[1]:
                    new_dots.add(((2*s[1]-d[0]),d[1]))
                else:
                    new_dots.add(d)
        else:
            for d in dots:
                if d[1] > s[1]:
                    new_dots.add((d[0],(2*s[1]-d[1])))
                else:
                    new_dots.add(d)
        dots = new_dots
        print(len(dots))
    max_x, max_y = 0, 0
    for d in dots:
        max_x = max(max_x, d[0])
        max_y = max(max_y, d[1])
    paper = []
    for i in range(max_y+1):
        paper.append([' ']*(max_x+1))
    for d in dots:
        paper[d[1]][d[0]] = '#'
    for l in paper:
        print(''.join(l))